import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

menu = [
  {title: 'Inicio', icon:'home', url:'home'},
  {title: 'Mi cuenta', icon:'person', url:'mi-cuenta'},
  {title: 'Mis Lugares', icon:'flag', url:'mis-lugares'},
  {title: 'Email y Notificaciones', icon:'notifications', url:'notificaciones'},
  {title: 'Propiedades Favoritas', icon:'heart', url:'propiedades-favoritas'},
  {title: 'Busquedas guardadas', icon:'search', url:'busquedas-guardadas'},
  {title: 'Vistos Recientemente', icon:'reload', url:'vistos-recientemente'},
  {title: 'Agentes', icon:'people', url:'agentes'},
  {title: 'oficinas', icon:'cafe', url:'oficinas'},
  {title: 'Aviso de Privacidad', icon:'shield-checkmark', url:'avisos'},
  {title: 'Terminos de Uso', icon:'menu', url:'terminos-uso'},
  {title: 'cerrar sesión', icon:'power', url:'inicio_sesion'},
]

  constructor() {}
}
