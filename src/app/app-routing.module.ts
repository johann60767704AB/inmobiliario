import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'mi-cuenta',
    loadChildren: () => import('./pages/mi-cuenta/mi-cuenta.module').then( m => m.MiCuentaPageModule)
  },
  {
    path: 'mis-lugares',
    loadChildren: () => import('./pages/mis-lugares/mis-lugares.module').then( m => m.MisLugaresPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./pages/notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },
  {
    path: 'propiedades-favoritas',
    loadChildren: () => import('./pages/propiedades-favoritas/propiedades-favoritas.module').then( m => m.PropiedadesFavoritasPageModule)
  },
  {
    path: 'busquedas-guardadas',
    loadChildren: () => import('./pages/busquedas-guardadas/busquedas-guardadas.module').then( m => m.BusquedasGuardadasPageModule)
  },
  {
    path: 'vistos-recientemente',
    loadChildren: () => import('./pages/vistos-recientemente/vistos-recientemente.module').then( m => m.VistosRecientementePageModule)
  },
  {
    path: 'agentes',
    loadChildren: () => import('./pages/agentes/agentes.module').then( m => m.AgentesPageModule)
  },
  {
    path: 'oficinas',
    loadChildren: () => import('./pages/oficinas/oficinas.module').then( m => m.OficinasPageModule)
  },
  {
    path: 'avisos',
    loadChildren: () => import('./pages/avisos/avisos.module').then( m => m.AvisosPageModule)
  },
  {
    path: 'terminos-uso',
    loadChildren: () => import('./pages/terminos-uso/terminos-uso.module').then( m => m.TerminosUsoPageModule)
  },
  {
    path: 'inicio-sesion',
    loadChildren: () => import('./pages/inicio-sesion/inicio-sesion.module').then( m => m.InicioSesionPageModule)
  },
  {
    path: 'detalles-mis-lugares',
    loadChildren: () => import('./pages/detalles-mis-lugares/detalles-mis-lugares.module').then( m => m.DetallesMisLugaresPageModule)
  },
 
  {
    path: 'lista',
    loadChildren: () => import('./pages/lista/lista.module').then( m => m.ListaPageModule)
  },
  {
    path: 'maps',
    loadChildren: () => import('./pages/maps/maps.module').then( m => m.MapsPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
