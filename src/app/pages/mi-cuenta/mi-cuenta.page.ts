import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mi-cuenta',
  templateUrl: './mi-cuenta.page.html',
  styleUrls: ['./mi-cuenta.page.scss'],
})
export class MiCuentaPage implements OnInit {

  datosU = [
    {title:'Nombre', description:'Johann', icon:''},
    {title:'Apellido', description:'Lukes', icon:''},
    {title:'Email', description:'andreasluke94@gmail.com', icon:''},
    {title:'Telefono', description:'5555555', icon:''},
    {title:'dirección', description:'Av. America', icon:''},
    {title:'Mi Agente Inmobiliario', description:'Jose Gonzalez', icon:''},
  ]
  constructor() { }

  ngOnInit() {
  }

}
