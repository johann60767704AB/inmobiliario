import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetallesMisLugaresPage } from './detalles-mis-lugares.page';

const routes: Routes = [
  {
    path: '',
    component: DetallesMisLugaresPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetallesMisLugaresPageRoutingModule {}
