import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetallesMisLugaresPageRoutingModule } from './detalles-mis-lugares-routing.module';

import { DetallesMisLugaresPage } from './detalles-mis-lugares.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetallesMisLugaresPageRoutingModule
  ],
  declarations: [DetallesMisLugaresPage]
})
export class DetallesMisLugaresPageModule {}
