import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VistosRecientementePageRoutingModule } from './vistos-recientemente-routing.module';

import { VistosRecientementePage } from './vistos-recientemente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VistosRecientementePageRoutingModule
  ],
  declarations: [VistosRecientementePage]
})
export class VistosRecientementePageModule {}
