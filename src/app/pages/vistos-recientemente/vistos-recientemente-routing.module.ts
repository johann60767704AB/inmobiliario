import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VistosRecientementePage } from './vistos-recientemente.page';

const routes: Routes = [
  {
    path: '',
    component: VistosRecientementePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VistosRecientementePageRoutingModule {}
