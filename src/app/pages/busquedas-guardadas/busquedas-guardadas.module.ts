import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusquedasGuardadasPageRoutingModule } from './busquedas-guardadas-routing.module';

import { BusquedasGuardadasPage } from './busquedas-guardadas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusquedasGuardadasPageRoutingModule
  ],
  declarations: [BusquedasGuardadasPage]
})
export class BusquedasGuardadasPageModule {}
