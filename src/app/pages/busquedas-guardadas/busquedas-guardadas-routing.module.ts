import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusquedasGuardadasPage } from './busquedas-guardadas.page';

const routes: Routes = [
  {
    path: '',
    component: BusquedasGuardadasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusquedasGuardadasPageRoutingModule {}
