import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-mis-lugares',
  templateUrl: './mis-lugares.page.html',
  styleUrls: ['./mis-lugares.page.scss'],
})
export class MisLugaresPage implements OnInit {

constructor(private router:Router ) { }

  ngOnInit() {
    
  }

  onclick(){
    this.router.navigate(['/detalles-mis-lugares']);
  }

}
