import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MisLugaresPageRoutingModule } from './mis-lugares-routing.module';

import { MisLugaresPage } from './mis-lugares.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MisLugaresPageRoutingModule
  ],
  declarations: [MisLugaresPage]
})
export class MisLugaresPageModule {}
