import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PropiedadesFavoritasPage } from './propiedades-favoritas.page';

const routes: Routes = [
  {
    path: '',
    component: PropiedadesFavoritasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PropiedadesFavoritasPageRoutingModule {}
