import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PropiedadesFavoritasPageRoutingModule } from './propiedades-favoritas-routing.module';

import { PropiedadesFavoritasPage } from './propiedades-favoritas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PropiedadesFavoritasPageRoutingModule
  ],
  declarations: [PropiedadesFavoritasPage]
})
export class PropiedadesFavoritasPageModule {}
